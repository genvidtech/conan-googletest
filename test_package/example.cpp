#include "gtest/gtest.h"

TEST(DummyTest, TestDummy) {
  EXPECT_EQ(1, 1); // still no quantum state ?
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
